#!/usr/bin/env python3

"""Fetch data from the Open Movie Database and export RDF statements from it"""

from pathlib import Path
import typing
import csv
import bz2
import urllib.parse
import itertools
import inspect
import os
import argparse
import time
import logging
from datetime import date

import requests
import rdflib

rdf = rdflib.Namespace('http://www.w3.org/1999/02/22-rdf-syntax-ns#')
foaf = rdflib.Namespace('http://xmlns.com/foaf/0.1/')
rdfs = rdflib.Namespace('http://www.w3.org/2000/01/rdf-schema#')
frbr = rdflib.Namespace('http://purl.org/vocab/frbr/core#')
omdb_movie = rdflib.Namespace('https://www.omdb.org/movie/')
omdb_category = rdflib.Namespace('https://www.omdb.org/encyclopedia/category/')
imdb_title = lambda x: rdflib.URIRef(f'http://www.imdb.com/title/{x}/')
schema = rdflib.Namespace('https://schema.org/')
dct = rdflib.Namespace('http://purl.org/dc/terms/')
skos = rdflib.Namespace('http://www.w3.org/2004/02/skos/core#')
exif = rdflib.Namespace('https://www.w3.org/2003/12/exif/ns#')

cachebase = Path(os.environ.get('XDG_CACHE_HOME', '~/.cache')).expanduser()

class OMDBDataFetcher:
    def __init__(self, *, freshness=3600):
        self.cachedir = cachebase / 'omdb-data'
        self.source_uri = 'https://www.omdb.org/data/'

        self.freshness = freshness

    def fetch_local(self, name: str) -> typing.TextIO:
        name = name + ".bz2"
        cachefile = self.cachedir / name
        if not cachefile.exists() or cachefile.stat().st_mtime + self.freshness < time.time():
            if cachefile.exists():
                if_modified_since = cachefile.stat().st_mtime
                if_modified_since = time.strftime('%a, %d %b %Y %H:%M:%S GMT', time.gmtime(if_modified_since))
            else:
                if_modified_since = None
            cachefile.parent.mkdir(exist_ok=True, parents=True)
            with requests.get(
                    self.source_uri + name,
                    stream=True,
                    headers={'If-Modified-Since': if_modified_since} if if_modified_since else {}) as response:
                if response.status_code == 304:
                    logging.info("File %s did not change since last check", name)
                    cachefile.touch()
                elif response.status_code == 200:
                    logging.info("Downloading %s", name)
                    tempfile = cachefile.parent / ('.' + cachefile.name + '.tmp')
                    with tempfile.open('wb') as outfile:
                        for data in response.iter_content():
                            outfile.write(data)
                    tempfile.rename(cachefile)
                else:
                    raise RuntimeError("Request to for %s unsuccessful: %s" % (name, response))
        # workaround for http://bugs.python.org/issue28268
        return bz2.open(cachefile.__fspath__(), 't')

    dialect = type('OMDBDialect', (csv.Dialect,), dict(delimiter=',', escapechar='\\', quoting=1, quotechar='"', lineterminator='\n'))

    def fetch_dict_reader(self, name: str) -> csv.DictReader:
        return csv.DictReader(self.fetch_local(name + '.csv'), dialect=self.dialect)

class Converter:
    def __init__(self, fetcher: OMDBDataFetcher, output_directory: Path, output_uri_base: rdflib.Namespace):
        self.fetcher = fetcher
        self.output_directory = output_directory
        self.output_uri_base = output_uri_base

        # still better than blank nodes, and no need to perform smushing
        self.omdb_movie_data = rdflib.Namespace(str(output_uri_base) + 'movies.nt#movie-')
        self.omdb_category_data = rdflib.Namespace(str(output_uri_base) + 'categories.nt#cat-')

    @property
    def output_uri(self):
        return self.output_uri_base[self.outfile]

    def gather(self, graph):
        doc = inspect.getdoc(self)
        if not doc:
            return

        label, sep, description = doc.partition('\n\n')
        this = rdflib.URIRef(self.output_uri)
        graph.add((this, rdfs.label, rdflib.Literal(label.strip())))
        if description:
            graph.add((this, rdfs.comment, rdflib.Literal(description.strip())))

    def run(self):
        g = rdflib.Graph()
        self.gather(g)
        g.serialize((self.output_directory / self.outfile).absolute().as_uri(), base=self.output_uri, format='nt')

class OMDBConverter(Converter):
    def gather(self, graph):
        super().gather(graph)

        this = rdflib.URIRef(self.output_uri)
        cc = rdflib.URIRef('http://creativecommons.org/licenses/by/2.0/de')
        omdb = rdflib.URIRef('https://www.omdb.org/')
        cma = rdflib.URIRef('http://christian.amsuess.com/#CMA')
        graph.add((this, dct.license, cc))
        graph.add((this, dct.source, omdb))
        graph.add((this, dct.rightsHolder, omdb))
        graph.add((this, dct.publisher, cma))
        graph.add((cma, foaf.mbox, rdflib.URIRef('mailto:chrysn@fsfe.org')))

class MovieLinksConverter(OMDBConverter):
    """Links from the Open Media Database (OMDB)

    These links were manually assigned to movies by OMDB users. Wikipedia links are sometimes still odd when it comes to encoding."""

    outfile = 'movie_links.nt'

    def gather(self, graph):
        super().gather(graph)

        for record in self.fetcher.fetch_dict_reader('movie_links'):
            if record['source'] in ('imdbmovie', 'imdbepisode'):
                other_movie_page = imdb_title(record['key'])
            elif record['source'] == 'wikipedia':
                # this is a bit fuzzy in omdb; this heuristic seems to work.
                #
                # this escaping around tries to get urls right to match what
                # wikipedia/wikidata is producing; should be verified, see
                # https://gitlab.com/chrysn/omdb-semantics/issues/1
                if '%' in record['key']:
                    key = urllib.parse.unquote(record['key'])
                else:
                    key = record['key']
                key = key.replace(' ', '_') # seems to be an outlier, but present in datasets as of 2017-10-17
                for c in '?"`':
                    key = key.replace(c, '%%%02X'%ord(c))
                other_movie_page = rdflib.URIRef(f"http://{record['language_iso_639_1']}.wikipedia.org/wiki/{key}")
            elif record['source'] == 'wikidata':
                continue
            elif record['source'] == 'moviepilot':
                other_movie_page = rdflib.URIRef(f"http://www.moviepilot.de/movies/{record['key']}")
            else:
                print("unknown source", record)
                continue

            graph.add((self.omdb_movie_data[record['movie_id']], foaf.isPrimaryTopicOf, other_movie_page))

class TrailersConverter(OMDBConverter):
    """Trailer from the Open Media Database (OMDB)

    These links were manually assigned to movies by OMDB users."""

    outfile = "trailers.nt"

    def gather(self, graph):
        super().gather(graph)

        for record in self.fetcher.fetch_dict_reader('trailers'):
            movie = self.omdb_movie_data[record['movie_id']]
            key = record['key']
            if record['source'] == 'generic':
                site_url = rdflib.URIRef(record['key'])
            else:
                if ':' in key:
                    ytprefix = 'http://www.youtube.com/watch?v='
                    assert record['source'] == 'youtube', "Non-generic but URL-like record found: %r" % record
                    assert key.startswith(ytprefix)
                    # FIXME: workaround for full uris that have made their way into the database, check with omdb whether that'll persist
                    key = key[len(ytprefix):]
                if record['source'] == 'youtube':
                    if '&' in key:
                        key = key[:key.index('&')]
                    site_url = rdflib.URIRef('http://www.youtube.com/watch?v=' + key)
                elif record['source'] == 'vimeo':
                    assert key.isdigit()
                    site_url = rdflib.URIRef('http://vimeo.com/' + key)
                else:
                    print("unknown trailer source", record['source'])
                    continue
            # FIXME see https://gitlab.com/chrysn/omdb-semantics/issues/2
            graph.add((movie, schema.trailer, site_url))

class ImagesConverter(OMDBConverter):
    """Movie images from the Open Media Database (OMDB)

    This models movies' images (other images are ignored) as foaf:depict(s)-ing
    them, and their smaller versions as thumbnails with their respective
    exif:imageWidth.

    Source and copyright metadata are extracted into DC terms.
    """

    outfile = "images.nt"

    KNOWN_LICENSES = {
            # As the license pages are the same for things like "Fair use /
            # movie poster" and "Fair use / DVD cover", the same links are used
            # for all of them; comments are there just in case there's a later
            # change to express the details in RDF as well.
            "44": rdflib.URIRef('https://de.omdb.org/content/License:FairUse'), # Movie poster
            "21": rdflib.URIRef('https://www.omdb.org/content/License:CC'),
            "0": None,
            "45": rdflib.URIRef('https://de.omdb.org/content/License:FairUse'), # DVD cover
            "31": rdflib.URIRef('https://de.omdb.org/content/License:PD:Expired'),
            "46": rdflib.URIRef('https://de.omdb.org/content/License:FairUse'), # no further details
            "33": rdflib.URIRef('https://www.omdb.org/content/License:PD:NRR'),
            "43": rdflib.URIRef('https://www.omdb.org/content/License:FairUse'), # no further details
            "47": rdflib.URIRef('https://www.omdb.org/content/License:FairUse'), # no further details
            "29": rdflib.URIRef('https://www.omdb.org/content/License:WC'),
            "48": rdflib.URIRef('https://www.omdb.org/content/License:FairUse'), # no further details
            "49": rdflib.URIRef('https://www.omdb.org/content/License:FairUse'), # Image of a dead person
            "41": rdflib.URIRef('https://www.omdb.org/content/License:FairUse'), # no further details
            "22": rdflib.URIRef('https://www.omdb.org/content/License:CC'), # but says "GNU free documentation"
            "42": rdflib.URIRef('https://www.omdb.org/content/License:FairUse'), # no further details
            "13": rdflib.Literal('provided by copyright holder for omdb'), # This one has no linked page
            "23": rdflib.URIRef('https://www.omdb.org/content/License:WC'),
            "12": rdflib.Literal('provided by a user'), # no linked page
            "32": rdflib.URIRef('https://www.omdb.org/content/License:PD:PRE1923'),
            }

    def gather(self, graph):
        super().gather(graph)

        movie_images = set()

        for record in self.fetcher.fetch_dict_reader('image_ids'):
            if record['object_type'] != 'Movie':
                continue

            movie_images.add(record['image_id'])

            movie = self.omdb_movie_data[record['object_id']]

            page = rdflib.URIRef('https://www.omdb.org/image/copyright/%s'%record['image_id'])
            default = rdflib.URIRef('https://www.omdb.org/image/default/%s.jpg'%record['image_id'])
            medium = rdflib.URIRef('https://www.omdb.org/image/medium/%s.jpg'%record['image_id'])
            small = rdflib.URIRef('https://www.omdb.org/image/small/%s.jpg'%record['image_id'])

            graph.add((default, foaf.depicts, movie))
            graph.add((default, exif.imageWidth, rdflib.Literal(200)))
            graph.add((default, foaf.isPrimaryTopicOf, page))
            graph.add((default, foaf.thumbnail, medium))
            graph.add((medium, exif.imageWidth, rdflib.Literal(92)))
            graph.add((default, foaf.thumbnail, small))
            graph.add((small, exif.imageWidth, rdflib.Literal(60)))

        for record in self.fetcher.fetch_dict_reader('image_licenses'):
            if record['image_id'] not in movie_images:
                # Don't have any other mentions, no use in having other metadata
                continue

            page = rdflib.URIRef('https://www.omdb.org/image/copyright/%s'%record['image_id'])
            default = rdflib.URIRef('https://www.omdb.org/image/default/%s.jpg'%record['image_id'])

            try:
                license = self.KNOWN_LICENSES[record['license_id']]
            except KeyError:
                raise KeyError("License %r not known, please look it up at %s" % (record['license_id'], page)) from None
            if record['source'] == 'N':
                source = None
            else:
                # Several links have surrounding whitespace; removing that may result in a valid link
                source = record['source'].strip()

                # This would be easier if it just raised instead of producing a warning
                from rdflib.term import _is_valid_uri
                if _is_valid_uri(source) and (source.startswith('http://') or source.startswith('https://')):
                    source = rdflib.URIRef(source)
                else:
                    # FIXME: Report this back
                    source = None
            if record['author'] == 'N':
                author = None
            else:
                author = rdflib.Literal(record['author'])

            if license is not None:
                graph.add((default, dct.rights, license))
            if source is not None:
                graph.add((default, dct.source, source))
            if author is not None:
                graph.add((default, dct.creator, author))

class CategoryConverter(OMDBConverter):
    """Categories and keywords from the Open Media Database (OMDB)

    This models the top-level categories (Genre, Audience, Filmmaking Movement
    as well as Keywords) as distinct Concept Schemes, and maps everything below
    it as Concepts. This works well for some categories (eg. genres), eg.
    "European Cinema" is broader than "French Cinema", but possibly not as well
    for others ("(Production Technical) Term / Camera / Camera Operation /
    Mobile Phone Camera" is not narrower than "Term / Camera", they are rather
    SKOS collections. Under the final remarks of skos-primer 4.1, ignoring that
    might work for some cases. Let me know if it bugs you so we can change
    this, and be prepared that things might change in that area.
    """
    outfile = "categories.nt"

    def gather(self, graph):
        super().gather(graph)

        schemes = set()

        for record in self.fetcher.fetch_dict_reader('all_categories'):
            if record['id'] == record['root_id']:
                schemes.add(record['id'])

        for record in self.fetcher.fetch_dict_reader('all_categories'):
            cat = self.omdb_category_data[record['id']]
            parent = self.omdb_category_data[record['parent_id']]
            cat_page = omdb_category[record['id']]
            root = self.omdb_category_data[record['root_id']]

            if record['id'] in schemes:
                graph.add((cat, rdf.type, skos.ConceptScheme))
            else:
                graph.add((cat, rdf.type, skos.Concept))
                graph.add((cat, skos.inScheme, root))
                if record['parent_id'] in schemes:
                    graph.add((parent, skos.hasTopConcept, cat))
                else:
                    graph.add((parent, skos.narrower, cat))

            graph.add((cat, foaf.isPrimaryTopicOf, cat_page))

        for record in self.fetcher.fetch_dict_reader('category_names'):
            cat = self.omdb_category_data[record['category_id']]
            name = rdflib.Literal(record['name'], lang=record['language_iso_639_1'])
            graph.add((cat, skos.prefLabel, name))

class MovieCategoryConverter(OMDBConverter):
    outfile = "movie-categories.nt"

    def gather(self, graph):
        super().gather(graph)

        for record in itertools.chain(
                self.fetcher.fetch_dict_reader('movie_categories'),
                self.fetcher.fetch_dict_reader('movie_keywords')
                ):
            movie = self.omdb_movie_data[record['movie_id']]
            cat = self.omdb_category_data[record['category_id']]
            graph.add((movie, dct.subject, cat))

class MoviesConverter(OMDBConverter):
    """Categories and keywords from the Open Media Database (OMDB)"""

    outfile = "movies.nt"

    def gather(self, graph):
        super().gather(graph)

        readers = [self.fetcher.fetch_dict_reader(x) for x in ('all_movies', 'all_movieseries', 'all_series', 'all_seasons', 'all_episodes')]
        for reader in readers:
            for record in reader:
                graph.add((self.omdb_movie_data[record['id']], rdfs.label, rdflib.Literal(record['name'])))
                if record['parent_id'] and record['parent_id'] != 'N':
                    graph.add((self.omdb_movie_data[record['parent_id']], frbr.part, self.omdb_movie_data[record['id']]))

                graph.add((self.omdb_movie_data[record['id']], foaf.isPrimaryTopicOf, omdb_movie[record['id']]))
                if record['date'] and record['date'] != 'N':
                    graph.add((self.omdb_movie_data[record['id']], schema.datePublished, rdflib.Literal(date.fromisoformat(record['date']))))


if __name__ == "__main__":
    p = argparse.ArgumentParser(description=__doc__)
    p.add_argument("--assume-freshness", help="Number of seconds to assume a download stays unmodified", default=3600, type=int)
    p.add_argument("-v", "--verbose", help="Produce debug output", action="count", default=0)
    p.add_argument('--output-directory', help="Local directory in which to create .nt files", default=Path('.'), type=Path)
    p.add_argument('--base-uri', help="Base URI to use for linking document metadata and creating URIs for OMDB objects", default=rdflib.Namespace(Path('.').absolute().as_uri() + '/'), type=rdflib.Namespace)
    args = p.parse_args()

    if args.verbose > 1:
        logging.basicConfig(level=logging.DEBUG)
    elif args.verbose:
        logging.basicConfig(level=logging.INFO)
    fetcher = OMDBDataFetcher(freshness=args.assume_freshness)
    converters = [
            MovieLinksConverter,
            TrailersConverter,
            ImagesConverter,
            CategoryConverter,
            MovieCategoryConverter,
            MoviesConverter,
            ]
    for c in converters:
        logging.info("Running %s", c.__name__)
        c(fetcher, args.output_directory, args.base_uri).run()
