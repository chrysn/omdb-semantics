omdb-semantics
==============

About this project
------------------

The purpose of this project is to make some of the truths published by OMDB_
available in the RDF_ data model. That data allows interconnecting movie data
with data from the `semantic web`_.

There is no intention to cover the complete database or to publish these
results in the omdb.org namespace – these would be interesting results, but
will require efforts from the omdb side which I currently don't have the time
to solicit or accompany. (Whoever wants to take this project as a starting
point for better integration is of course free to do so.)

The output files of this program are regularly `published via GitLab Pages`_;
those builds are, however, experimental, and should not be relied on.

Being out of the name space, no persistent URIs are minted for the things
behind the OMDB pages. Instead of blank nodes everywhere, local file:// URIs
are used that make it unnecessary to add foaf_:isPrimaryTopicOf statements to
all exported datasets just for smushing between them; those are to be
considered volatile.

.. _RDF: https://en.wikipedia.org/wiki/Resource_Description_Framework
.. _`semantic web`: http://linkeddata.org/
.. _OMDB: https://www.omdb.org/
.. _`published via GitLab Pages`: https://chrysn.gitlab.io/omdb-semantics/

Typical statements
------------------

These statements (shown here in Turtle) are typical output::

    @prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
    @prefix schema: <https://schema.org/> .
    @prefix movies: <file:///.../movies.nt#movie-> .
    @prefix foaf: <http://xmlns.com/foaf/0.1/> .
    
    movies:10378
        foaf:isPrimaryTopicOf <http://de.wikipedia.org/wiki/Big_Buck_Bunny>,
            <http://en.wikipedia.org/wiki/Big_Buck_Bunny>,
            <http://es.wikipedia.org/wiki/Big_Buck_Bunny>,
            <http://fr.wikipedia.org/wiki/Big_Buck_Bunny>,
            <http://www.imdb.com/title/tt1254207/>,
            <https://www.omdb.org/movie/10378> ;
        schema:trailer <http://www.youtube.com/watch?v=YE7VzlLtp-4> .

The actual output of the program is in NT_ format for performance reasons.

Vocabularies used
-----------------

* foaf_:isPrimaryTopicOf: between movies and both external links (Wikipedia,
  IMDb) and OMDB pages (Beware that OMDB issues URIs that have optional and
  ignored data in them; the relation to ``https://www.omdb.org/movie/787`` is
  stated, the one to ``https://www.omdb.org/movie/787-mr-mrs-smith`` is not.)

* rdfs_:label for the movies' original titles without any locale attached.

* frbr_:part for the movie nesting in OMDB that keeps together both series,
  seasons and episodes as well as franchises, series and movies.

  Note that while frbr:parent can be used between Endeavors. Thus, this export
  does not make any statement on which (WEMI) class the movies are actually in;
  this is likely to only change if OMDB starts modelling movie versions (cuts /
  editions / different dubs); in that case. (For any attempts to start this, I
  suggest reading Karen Coyle's `FRBR critique`_ before.)

* schema_:trailer for trailers (or their pages – see `#2`_).

* dct_:subject for categories and keywords, which are themselves organized as
  skos_:ConceptScheme and skos_:Concept with skos_:narrower statements
  connecting them.

.. _foaf: http://xmlns.com/foaf/0.1/
.. _rdfs: http://www.w3.org/2000/01/rdf-schema#
.. _frbr: http://purl.org/vocab/frbr/core#
.. _`FRBR critique`: http://kcoyle.net/frbr/?page_id=102
.. _schema: https://schema.org/
.. _`#2`: https://gitlab.com/chrysn/omdb-semantics/issues/2
.. _dct: http://purl.org/dc/terms/
.. _skos: http://www.w3.org/2004/02/skos/core#

About the software
------------------

The ``omdb-semantics.py`` program fetches the required dump files from OMDB and
converts them to NT_ files in the current directory. It depends on Python_ 3.6
and rdflib_.

The program is written by chrysn_ and published under the terms of the LGPLv3_
or any later version. You are invited to contribute to the project on its
`gitlab page`_.

This downloads and processes data made available by OMDB under their license
terms; see their `copyright page`_ for details.

.. _NT: https://en.wikipedia.org/wiki/N-Triples
.. _Python: https://www.python.org/
.. _rdflib: https://github.com/RDFLib/rdflib/
.. _chrysn: mailto:chrysn@fsfe.org
.. _LGPLv3: http://www.gnu.org/licenses/lgpl-3.0
.. _`gitlab page`: https://gitlab.com/chrysn/omdb-semantics
.. _`copyright page`: https://www.omdb.org/content/Copyright
